//
//  ViewController.swift
//  cofee-lucas-rodrigues-1
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Kingfisher
import Alamofire

struct Cofee: Decodable{
    let file: String
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        getNewCofee()
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var ImageView: UIImageView!
    @IBAction func btn_recarregar(_ sender: Any) {
        getNewCofee()
    }
    
    func getNewCofee(){
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of:Cofee.self ){
            response in
            if let cofee = response.value{
                self.ImageView.kf.setImage(with: URL(string: cofee.file))
            }
        }
    }
    
}

